import React, { useContext } from "react";
import { DarkTheme } from "./ThemeChange";

function Filter({
  getRegionArray,
  handleRegion,
  getSubRegionArray,
  handleSubRegion,
  subregion,
  handlePopulation,
  population,
  handleArea,
  Area,
  handleSearch

}) {
  const { darkMode, toggleDarkMode } = useContext(DarkTheme);
 
  return (
    <div
      className={`flex justify-between p-3 ${
        darkMode ? "bg-black text-white" : "bg-white text-black"
      } `}
    >
      <input
        type="text"
        placeholder="Search for a country"
        className={`p-3 shadow-md ${
          darkMode ? "bg-gray-700 text-white" : "bg-white text-black"
        }`}
         onChange={handleSearch}
      ></input>
      {/* -----region_----- */}
      <select
        name="region"
        id="region"
        className={`p-3 shadow-md ${
          darkMode ? "bg-gray-700 text-white " : "bg-white text-black"
        }`}
         onChange={handleRegion}
      >
        <option hidden value="">
          Filter by Region
        </option>
        <option value="all">ALL</option>
        {getRegionArray &&
          getRegionArray.map((item, index) => {
            return (
              <option key={index} value={item}>
                {item}
              </option>
            );
          })}
      </select>
      {/* ------subRegion----- */}
      <select
      value={subregion}
        className={`p-3 shadow-md ${
          darkMode ? "bg-gray-700 text-white" : "bg-white text-black"
        } ${getSubRegionArray.length > 0 ? "block" : "hidden"}`}
        onChange={handleSubRegion}
      >
        <option hidden value="">
          Filter by Subregion
        </option>
        {getSubRegionArray &&
          getSubRegionArray.map((subregion, index) => (
            <option key={index} value={subregion}>
              {subregion}
            </option>
          ))}
      </select>
      {/* ----population */}
      <select 
      value={population}
        className={`p-3 shadow-md ${
          darkMode ? "bg-gray-700 text-white" : "bg-white text-black"
        }`}
        onChange={handlePopulation}
      >
        <option hidden value="">
          Filter by population
        </option>
        <option value="Ascending">Ascending</option>
        <option value="Descending">Descending</option>
      </select>
      {/* ---Area---- */}
      <select
      value={Area}
        className={`p-3 shadow-md ${
          darkMode ? "bg-gray-700 text-white" : "bg-white text-black"
        }`}
        onChange={handleArea}
      >
        <option hidden value="">
          Filter by Area
        </option>
        <option value="Ascending">Ascending</option>
        <option value="Descending">Descending</option>
      </select>
    </div>
  );
}

export default Filter;
