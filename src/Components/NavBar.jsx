import React, { useContext, useState } from "react";
import { MdOutlineDarkMode } from "react-icons/md";
import { DarkTheme } from "./ThemeChange";
import { Link } from "react-router-dom";

function NavBar() {
  const { darkMode, toggleDarkMode } = useContext(DarkTheme);
  return (
    <div
      className={`${
        darkMode
          ? "bg-black text-white border-2 border-gray-600"
          : "bg-white text-black"
      }`}
    >
      <nav className="navbar  bg-light shadow-md p-3 flex justify-between">
        <Link to="/">
          <h1 className="font-bold text-3xl"> Where in the word?</h1>
        </Link>

        <button className="flex items-center" onClick={toggleDarkMode}>
          <MdOutlineDarkMode />
          Dark Mode
        </button>
      </nav>
    </div>
  );
}

export default NavBar;
