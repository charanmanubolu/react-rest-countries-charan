import { useState,  createContext} from "react";

export let DarkTheme = createContext()
export default function ThemeChange({children}) {
    const [darkMode , setDarkMode] = useState(false)
      
    function toggleDarkMode(){
     setDarkMode(!darkMode)
    }
    return(
     <DarkTheme.Provider value={{darkMode,toggleDarkMode}}>
       {children}
     </DarkTheme.Provider>
    )
   }

