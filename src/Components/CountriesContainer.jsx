import { useContext } from 'react';
import { DarkTheme } from './ThemeChange';
import { Link } from 'react-router-dom';

function HeroContainer({ data }) {
  const { darkMode, toggleDarkMode } = useContext(DarkTheme);

  if (!data) {
    return null; 
  }


  return (
    <div className={`${darkMode ? 'bg-black text-white' : 'bg-white text-black'} min-h-[100vh] `}>
      <div className="flex flex-wrap justify-between w-full">
        {data.map((country,index) => (
          <div  className={`w-1/5 m-5 shadow-md rounded-xl p-2 ${darkMode ? 'bg-gray-700 text-white' : 'bg-white text-black'}`} id='card-container' key={country.id}>
           
            {country.flags && country.name &&
              <div key={index}>
              <Link to={`/country/${country.cca3}`}>
                <img className='h-[200px] mb-2' src={country.flags.png} alt={country.name.common} />
                <h1 className="text-2xl font-bold">{country.name.common}</h1>
                <p><b>Population:</b> {country.population}</p>
                <p><b>Region:</b> {country.region}</p>
                <p><b>Capital:</b> {country.capital}</p>
                </Link>
              </div>
              
            }
          </div>
        ))}
      </div>
    </div>
  );
}

export default HeroContainer;
