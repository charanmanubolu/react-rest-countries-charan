import React, { useEffect, useState,useContext } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { DarkTheme } from "../Components/ThemeChange";
import { GoArrowLeft } from "react-icons/go";
import NotFound from "./NotFound";
import { getCountriesData } from "../Data/data";

function detail() {
  let { cca3 } = useParams();
  let [data, setData] = useState([]);
  let { darkMode, toggleDarkMode } = useContext(DarkTheme);
  const [loader, setLoader] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    async function CountriesData() {
      try {
        let data = await getCountriesData();
        let OneCountryData = data.filter((country) => country.cca3 === cca3);
        setData(OneCountryData)
        // setData(data);
        setLoader(false);
      } catch (err) {
        setError(err.message);
      }
    }
    CountriesData();
  }, [cca3]);
  if (loader) {
    return (<>
    <div id='loader'></div>
    </>)
  }


  return (
    <div className={`${darkMode ? 'bg-black text-white' : 'bg-white text-black' } h-[100vh]`}>
  
      <button
        className="border-2 border-black  p-3 flex align-center ml-5"
        onClick={() => window.history.back()} 
      >
     
        <GoArrowLeft/>
        Go Back
      </button>
    
    { error.length>0 ? <NotFound/> :
      data.length === 0 ? (
        <div className="loader"></div>
      ) :(

      <div key={data[0]?.name.common} className="flex m-5 pb-28">
      <img
        className="w-[45%]"
        src={`${data[0]?.flags.png}`}
        alt={`${data[0]?.name.common}`}
      />
      <div className="w-[45%] p-10">
        <h2 className="text-4xl font-bold p-2">{data[0]?.name.common}</h2>
        <p class="p-2">
          <b>Capital:</b> {data[0]?.capital}
        </p>
        <p class="p-2">
          <b>Region:</b> {data[0]?.region}
        </p>
        <p class="p-2">
          <b>Subregion:</b> {data[0]?.subregion}
        </p>
        <p class="p-2">
          <b>Population:</b> {data[0]?.population}
        </p>
        <p class="p-2">
          <b>Top Level Domain:</b> {data[0]?.tld[0]}
        </p>
        <p class="p-2">
          <b>Currencies : </b>
          {data[0] ? Object.keys(data[0]?.currencies) : ""}
        </p>
        <p class="p-2">
          <b>Languages : </b>
          {data[0] ? Object.keys(data[0]?.languages).join(", ") : ""}
        </p>
        <ul className="flex">
          <b>Border : </b>
          {data[0]?.borders
            ? data[0]?.borders.map((element, index) => {
                return (
                  <Link
                    className="border-2 border-black m-5"
                    to={`/country/${element}`}
                  >
                    <button className="px-2 py-1">{element}</button>
                    
                  </Link>
                );
              })
            : " No borders"}
        </ul>
      </div>
    </div>
      )
    }
    </div>
  );
}

export default detail;
