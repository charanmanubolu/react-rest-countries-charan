import Filter from "../Components/InputSearch";
import HeroContainer from "../Components/CountriesContainer";
import { useState, useEffect } from "react";
import { getCountriesData } from "../Data/data";
import NotFound from "./NotFound";
import React from "react";

function HomePage() {
  const [data, setData] = useState([]);
  const [region, setRegion] = useState("");
  const [subregion, setSubRegion] = useState("");
  const [population, setPopulation] = useState("");
  const [Area, setArea] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const [loader, setLoader] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    async function CountriesData() {
      try {
        let data = await getCountriesData();
        setData(data);
        setLoader(false);
      } catch (err) {
        setError(err.message);
      }
    }
    CountriesData();
  }, []);

  let getRegionArray = data
    .reduce((acc, cv) => {
      if (!acc.includes(cv.region)) {
        acc.push(cv.region);
      }
      return acc;
    }, [])
    .sort();

  function handleRegion(e) {
    let regionValue = e.target.value;
    setRegion(regionValue);
    setSubRegion("");
    setPopulation("");
    setArea("");
  }
  let getSubRegionArray = data
    .reduce((acc, current) => {
      if (current.region == region) {
        if (!acc.includes(current.subregion)) {
          acc.push(current.subregion);
        }
      }

      return acc;
    }, [])
    .sort();

  function handleSubRegion(e) {
    setSubRegion(e.target.value);
  }

  function handlePopulation(e) {
    setPopulation(e.target.value);
  }
  function handleArea(e) {
    setArea(e.target.value);
  }
  function handleSearch(e) {
    setSearchInput(e.target.value);
  }

  function filterData() {
    let filteredData = data;

    if (region == "") {
      filteredData = data;
    } else {
      let regionData = data.filter((country) => country.region === region);
      filteredData = regionData;
    }

    if (subregion == "") {
      filteredData = filteredData;
    } else {
      let subregionData = filteredData.filter(
        (country) => country.subregion === subregion
      );
      filteredData = subregionData;
    }

    if (population == "Ascending") {
      let ascending = filteredData.sort((a, b) => a.population - b.population);
      filteredData = ascending;
    } else if (population == "Descending") {
      let descending = filteredData.sort((a, b) => b.population - a.population);
      filteredData = descending;
    }

    if (Area == "Ascending") {
      let ascending = filteredData.sort((a, b) => a.area - b.area);
      filteredData = ascending;
    } else if (Area == "Descending") {
      let descending = filteredData.sort((a, b) => b.area - a.area);
      filteredData = descending;
    }

    if (searchInput) {
      filteredData = filteredData.filter((country) =>
        country.name.common
          .toLowerCase()
          .includes(searchInput.toLocaleLowerCase())
      );
    }
    return filteredData;
  }

  let filteredData = filterData();
  if (loader) {
    return <div id="loader"></div>;
  }
  if (error) {
    return (
      <>
        <NotFound />
      </>
    );
  }
  return (
    <>
      <Filter
        getRegionArray={getRegionArray}
        handleRegion={handleRegion}
        getSubRegionArray={getSubRegionArray}
        handleSubRegion={handleSubRegion}
        subregion={subregion}
        handlePopulation={handlePopulation}
        population={population}
        handleArea={handleArea}
        Area={Area}
        handleSearch={handleSearch}
      />
      {filteredData.length > 0 ? (
        <HeroContainer data={filteredData} />
      ) : (
        <h1 className="text-5xl font-bold ">NO Data Found</h1>
      )}
    </>
  );
}

export default HomePage;
