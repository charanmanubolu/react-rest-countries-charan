import { BrowserRouter, Route, Routes } from "react-router-dom";
import Detail from "./pages/DetailsContainer";
import ThemeChange from "./Components/ThemeChange";
import NavBar from "./Components/NavBar";
import NotFound from "./pages/NotFound"
import HomePage from "./pages/HomePage";

function App() {
  return (
    <>
      <ThemeChange>        
        <BrowserRouter>
        <NavBar />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/country/:cca3" element={<Detail />} />
            <Route path='*' element={<NotFound/>} />
          </Routes>
        </BrowserRouter>
      </ThemeChange>
    </>
  );
}

export default App;
